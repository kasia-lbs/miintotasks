﻿using MiintoTasks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiintoTasks.Models
{
    public class BrandModel
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public static IEnumerable<BrandModel> GetBrands(IRepository repository)
        {
            return repository.GetBrands().Select(b => new BrandModel()
            {
                Code = b.Code,
                Name = b.Name
            });
        }

    }
}