﻿using MiintoTasks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiintoTasks.Models
{
    public class ProductModel
    {
        public string Brand { get; set; }

        public string BrandCode { get; set; }

        public int Stock { get; set; }

        public string ProductTitle { get; set; }

        public string Category { get; set; }

        public string ThumbnailPath {get; set;}


        public static IEnumerable<ProductModel> GetProducts(IEnumerable<ProductDto> products)
        {
            return products.Select(b => new ProductModel()
            {
                Brand = b.Brand,
                BrandCode = b.BrandCode,
                Stock = b.Stock,
                ProductTitle = b.ProductTitle,
                Category = b.Category,
                ThumbnailPath = b.ThumbnailPath
            });
        }
    }
}