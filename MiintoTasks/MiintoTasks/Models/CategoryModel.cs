﻿using MiintoTasks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiintoTasks.Models
{
    public class CategoryModel
    {
        public string Name { get; set; }

        public static IEnumerable<CategoryModel> GetCategory(IRepository repository)
        {
            return repository.GetCategory().Select(b => new CategoryModel()
            {
                Name = b.Name
            });
        }
    }
}