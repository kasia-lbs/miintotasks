﻿using MiintoTasks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiintoTasks.Models
{
    public class ShowProductsModel
    {
        public string SelectedBrand { get; set; }

        public string SelectedCategory { get; set; }


        public IEnumerable<BrandModel> AllBrands { get; set; }

        public IEnumerable<CategoryModel> AllCategory { get; set; }

        public static ShowProductsModel GetShowProductsModel(IRepository repository)
        {
            var result = new ShowProductsModel();

            var brands = new List<BrandModel>()
            {
                new BrandModel()
            };
            brands.AddRange(BrandModel.GetBrands(repository));            
            result.AllBrands = brands;

            var categories = new List<CategoryModel>()
            {
                new CategoryModel()
            };
            categories.AddRange(CategoryModel.GetCategory(repository));
            result.AllCategory = categories;

            return result;
        }

    }
}