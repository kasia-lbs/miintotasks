﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MiintoTasks
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "CssTask", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CssTask",
                url: "CssTask",
                defaults: new { controller = "CssTask", action = "Index", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "NetTask",
                url: "NetTask",
                defaults: new { controller = "NetTask", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
