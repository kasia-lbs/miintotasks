﻿using MiintoTasks.Data;
using MiintoTasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiintoTasks.Controllers
{
    public class NetTaskController : Controller
    {
        private readonly IRepository m_repository;

        public NetTaskController(IRepository repository)
        {
            m_repository = repository;
        }

        // GET: NetTask
        public ActionResult Index()
        {
            ShowProductsModel model = ShowProductsModel.GetShowProductsModel(m_repository);
            return View(model);
        }
    }
}