﻿using MiintoTasks.Data;
using MiintoTasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiintoTasks.Controllers
{
    public class NetTaskApiController : ApiController
    {
        private readonly IRepository m_repository;

        public NetTaskApiController(IRepository repository)
        {
            m_repository = repository;
        }

        public IEnumerable<ProductModel> GetProducts(string brand = null, string category = null)
        {
            return ProductModel.GetProducts(m_repository.GetProducts(new ProductQueryDto()
            {
                BrandCode = brand,
                Category = category
            }));
        }
    }
}
