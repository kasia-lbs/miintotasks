﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MiintoTasks.Startup))]

namespace MiintoTasks
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // intentionally emtpy
        }
    }
}
