﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace MiintoTasks.Data
{
    public interface IRepository
    {
        BrandDto[] GetBrands();

        CategoryDto[] GetCategory();

        ProductDto[] GetProducts(ProductQueryDto query);

    }

    public class Repository : IRepository
    {
        // TODO: put these in web config and inject somehow into Repository
        const string BrandsUrl = "http://demo6036677.mockable.io/api/brands";
        const string CategoryUrl = "http://demo6036677.mockable.io/api/categories";
        const string ProductUrl = "http://demo6036677.mockable.io/api/products";

        private TDto[] GetDto<TDto>(string url)
        {
            using (WebClient client = new WebClient())
            {
                string content = client.DownloadString(url);
                return JsonConvert.DeserializeObject<TDto[]>(content);
            }
            
        }

        public BrandDto[] GetBrands()
        {
            return GetDto<BrandDto>(BrandsUrl);
        }

        public CategoryDto[] GetCategory()
        {
            return GetDto<string>(CategoryUrl).Select(n => new CategoryDto() { Name = n }).ToArray();
        }

        public ProductDto[] GetProducts(ProductQueryDto query)
        {
            IEnumerable<ProductDto> products = GetDto<ProductDto>(ProductUrl).Where(p => p.Stock > 0);

            if(query != null && query.BrandCode != null)
            {
                products = products.Where(p => p.BrandCode == query.BrandCode);
            }

            if (query != null && query.Category != null)
            {
                products = products.Where(p => p.Category == query.Category);
            }

            return products.ToArray();
        }
    }
}