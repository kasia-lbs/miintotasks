﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiintoTasks.Data
{
    public class ProductDto
    {
        public string Brand { get; set; }

        public string BrandCode { get; set; }

        public int Stock { get; set; }

        public string ProductTitle { get; set; }

        public string Category { get; set; }

        public string ThumbnailPath { get; set; }
    }
}