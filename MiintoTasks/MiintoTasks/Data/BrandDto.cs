﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiintoTasks.Data
{
    public class BrandDto
    {
        public string Name { get; set; }

        public string Code { get; set; }
    }
}