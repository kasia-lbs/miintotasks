﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiintoTasks.Data
{
    public class ProductQueryDto
    {
        public string BrandCode { get; set; }

        public string Category { get; set; }
    }
}