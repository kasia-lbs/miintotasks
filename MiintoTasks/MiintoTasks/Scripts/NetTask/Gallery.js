/// <reference path="../jquery.d.ts" />
/// <reference path="../knockout.d.ts" />
var GalleryViewModel = (function () {
    function GalleryViewModel() {
        this.brand = ko.observable();
        this.category = ko.observable();
        this.products = ko.observableArray();
    }
    GalleryViewModel.prototype.attach = function (panelId) {
        var self = this;
        this.brand.subscribe(function () { return self.onFilterChanged(); });
        this.category.subscribe(function () { return self.onFilterChanged(); });
        ko.applyBindings(this, document.getElementById(panelId));
    };
    GalleryViewModel.prototype.onFilterChanged = function () {
        var url = "/api/getProducts";
        var brand = this.brand();
        var category = this.category();
        if (brand || category) {
            url = url + "?" + $.param({
                brand: brand,
                category: category
            });
        }
        var self = this;
        $.get(url, function (data) { return self.setProducts(data); });
    };
    GalleryViewModel.prototype.setProducts = function (products) {
        this.products(products);
    };
    return GalleryViewModel;
})();
function attachGalleryViewModel() {
    var viewModel = new GalleryViewModel();
    viewModel.attach('galleryContainer');
}
$(attachGalleryViewModel);
//# sourceMappingURL=Gallery.js.map