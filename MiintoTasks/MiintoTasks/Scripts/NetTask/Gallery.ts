﻿/// <reference path="../jquery.d.ts" />
/// <reference path="../knockout.d.ts" />

class GalleryViewModel {
    public brand = ko.observable();
    public category = ko.observable();
    public products = ko.observableArray();
    
    public attach(panelId: string) {
        var self = this;
        this.brand.subscribe(() => self.onFilterChanged());
        this.category.subscribe(() => self.onFilterChanged());

        ko.applyBindings(this, document.getElementById(panelId));
    }

    private onFilterChanged() {
        var url = "/api/getProducts";

        var brand = this.brand();
        var category = this.category();
        if (brand || category) {
            url = url + "?" + $.param({
                brand: brand,
                category: category
            });
        }

        var self = this;
        $.get(url, data => self.setProducts(data));
    }

    private setProducts(products) {
        this.products(products);
    }
}

function attachGalleryViewModel() {
    var viewModel = new GalleryViewModel();

    viewModel.attach('galleryContainer');
}

$(attachGalleryViewModel);